<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'content' => $this->content,
            'user' => array_map(
                function ($user) {
                    return $user['name'];
                },
                $this->users->toArray()
            ),
            'img_url' => 'https://i.pravatar.cc',
        ];
    }
}
